﻿using Api.Models;
using Api.Constatns;
using Api.Dtos.Employee;
using Api.Dtos.Dependent;

namespace Api.Calcularors
{
    public static class PaycheckCalculator
    {
        public static Paycheck Calculate(this GetEmployeeDto? employee, int paymentNumber, int year)
        {
            if (employee == null)
            {
                throw new ArgumentNullException(nameof(employee));
            }

            var paymentDate = GetPaymentDate(paymentNumber, year);
            var paycheck = new Paycheck()
            {
                Id = paymentNumber,
                DateOfCalculation = paymentDate,
                EmployeeId = employee.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                PaySum = GetPaySum(employee.Salary),
                BaseCost = IsMonthBenefistRequired(paymentNumber) ? Constant.BaseCostPerMonth : 0.0m,
                DependentsCost = IsMonthBenefistRequired(paymentNumber) ? GetDependentsCost(employee.Dependents) : 0.0m,
                ExceedingCost = IsMonthBenefistRequired(paymentNumber) ? GetExceedingCost(employee.Salary) : 0.0m,
                DependentAgeCost = IsMonthBenefistRequired(paymentNumber) ? GetDependentAgeCost(employee.Dependents, paymentDate) : 0.0m
            };

            return paycheck;
        }

        // 2 times per month plus 2 annual benefits
        private static decimal GetPaySum(decimal salary)
        {
            return salary / Constant.NumberPaychecksPerYear;
        }

        // $600 for each dependent
        private static decimal GetDependentsCost(ICollection<GetDependentDto> dependents)
        {
            return dependents.Count() * Constant.DependentAdditionalCostPerMonth;
        }

        // if salary > 80000
        private static decimal GetExceedingCost(decimal salary)
        {
            return (salary > Constant.SalaryLimitation) ? salary * Constant.PercentOfBenefitCost : 0.0m;
        }

        // for dependents with age > 50 years
        private static decimal GetDependentAgeCost(ICollection<GetDependentDto> dependents, DateTime paymentDate)
        {
             return dependents.Where(x => GetAge(x.DateOfBirth, paymentDate) > Constant.DependentBenefitAge)
                .Count() * Constant.DependentBenefitAgeSum;
        }

        private static int GetAge(DateTime dateOfBirth, DateTime paymentDate)
        {
            return paymentDate.Year - dateOfBirth.Year;
        }

        //Last 2 payments and second payment of Month are without costs for benefits
        private static bool IsMonthBenefistRequired(int paymentNumber)
        {
            return !(paymentNumber % 2 == 0 || paymentNumber > Constant.NumberPaychecksPerYear - 2);
        }

        private static DateTime GetPaymentDate(int paymentNumber, int year)
        {
            int month;
            int day;

            if (paymentNumber > Constant.NumberPaychecksPerYear - 2)
            {
                month = 12;
                day = 31;
            }
            else
            {
                if (IsMonthBenefistRequired(paymentNumber))
                {
                    month = (paymentNumber + 1) / 2;
                    day = 15;
                }
                else
                {
                    month = paymentNumber / 2;
                    day = (month == 12)? 31 : new DateTime(year, month + 1, 1).AddDays(-1).Day; //Get Last Day of the Month
                }
            }

            return new DateTime(year, month, day);
        }
    }
}
