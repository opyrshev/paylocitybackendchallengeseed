﻿using Api.Models;
using Api.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaychecksController : ControllerBase
    {
        private readonly IPaycheckServise _paycheckServise;

        public PaychecksController(IPaycheckServise paycheckServise)
        {
            _paycheckServise = paycheckServise;
        }

        [SwaggerOperation(Summary = "Get all employees annual paychecks")]
        [HttpGet("{year}")]
        public async Task<ActionResult<ApiResponse<List<Paycheck>>>> GetEmployeesPaychecks(int year)
        {
            var result = await _paycheckServise.GetEmployeesPaychecks( year);

            return Ok(result);
        }

        [SwaggerOperation(Summary = "Get employee annual paychecks")]
        [HttpGet("{id}/{year}")]
        public async Task<ActionResult<ApiResponse<List<Paycheck>>>> GetEmployeePaychecks(int id, int year)
        {
            var result = await _paycheckServise.GetEmployeePaychecks(id, year);

            return Ok(result);
        }
    }
}
