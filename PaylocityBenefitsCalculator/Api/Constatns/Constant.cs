﻿namespace Api.Constatns
{
    public static class Constant
    {
        public const int NumberPaychecksPerYear = 26;

        public const decimal BaseCostPerMonth = 1000.0m;

        public const decimal DependentAdditionalCostPerMonth = 600.0m;

        public const decimal SalaryLimitation = 80000.0m;

        public const decimal PercentOfBenefitCost = 0.02m;

        public const int DependentBenefitAge = 50;

        public const decimal DependentBenefitAgeSum = 200.0m;
    }
}
