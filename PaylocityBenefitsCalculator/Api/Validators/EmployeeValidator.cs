﻿using Api.Dtos.Dependent;
using Api.Dtos.Employee;
using Api.Models;

namespace Api.Validators
{
    public static class EmployeeValidator
    {
        public static ApiResponse<GetEmployeeDto> Validate(this GetEmployeeDto? employee)
        {
            if (employee == null)
            {
                return new ApiResponse<GetEmployeeDto>()
                {
                    Success = false,
                    Error = "NotFound"
                };
            }
            var result = new ApiResponse<GetEmployeeDto>();
            result.Data = employee;
            var partnersCount = GetPartnersCount(employee.Dependents);
            if (partnersCount > 1)
            {
                result.Success = false;
                result.Error = "IncorrectData";
                result.Message = $"Employee {employee.FirstName} {employee.LastName} has {partnersCount} partners";
            }
            else
            {
                result.Success = true;
            }

            return result;
        }

        public static ApiResponse<List<GetEmployeeDto>> Validate(this List<GetEmployeeDto> employees)
        {
            if (employees == null || employees.Count() == 0)
            {
                return new ApiResponse<List<GetEmployeeDto>>()
                {
                    Success = false,
                    Error = "NotFound"
                };
            }

            var checkedEmployees = employees.Select(x => x.Validate()).ToList();

            var result = new ApiResponse<List<GetEmployeeDto>>()
            {
                Data = employees,
                Success = !checkedEmployees.Where(x => !x.Success).Any(),
                Error = string.Join(';', checkedEmployees.Where(x => !string.IsNullOrEmpty(x.Error)).Select(x => x.Error)),
                Message = string.Join(';', checkedEmployees.Where(x => !string.IsNullOrEmpty(x.Message)).Select(x => x.Message))
            };

            return result;
        }

        private static int GetPartnersCount(ICollection<GetDependentDto> dependents)
        {
            return dependents
                .Where(x => x.Relationship == Relationship.Spouse || x.Relationship == Relationship.DomesticPartner)
                .Count();
        }
    }
}
