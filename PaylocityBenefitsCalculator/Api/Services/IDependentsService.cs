﻿using Api.Dtos.Dependent;
using Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Services
{
    public interface IDependentsService
    {
        Task<ApiResponse<GetDependentDto>> GetDependentResponse(int i);

        Task<ApiResponse<List<GetDependentDto>>> GetDependentResponses();
    }
}
