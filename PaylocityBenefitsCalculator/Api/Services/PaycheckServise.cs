﻿using Api.Calcularors;
using Api.Constatns;
using Api.Models;

namespace Api.Services
{
    public class PaycheckServise : IPaycheckServise
    {
        private readonly IEmployeesService _employeesService;

        public PaycheckServise(IEmployeesService employeesService)
        {
            _employeesService = employeesService;
        }

        public async Task<ApiResponse<List<Paycheck>>> GetEmployeePaychecks(int id, int year)
        {
            var employeeResponse = await _employeesService.GetEmployeeResponse(id);
            var paychecks = new List<Paycheck>();
            if (employeeResponse.Success)
            {
                var employee = employeeResponse.Data;
                for (int i = 1; i <= Constant.NumberPaychecksPerYear; i++)
                {
                    paychecks.Add(employee.Calculate(i, year));
                }
            }
            else
            {
                paychecks = null;
            }
            var result = new ApiResponse<List<Paycheck>>()
            {
                Data = paychecks,
                Success = employeeResponse.Success,
                Error = employeeResponse.Error,
                Message = employeeResponse.Message
            };

            return await Task.FromResult(result);
        }

        public async Task<ApiResponse<List<Paycheck>>> GetEmployeesPaychecks( int year)
        {
            var employeesResponse = await _employeesService.GetEmployeeResponses();
            var paychecks = new List<Paycheck>();

            if (employeesResponse.Success && employeesResponse.Data != null)
            {
                foreach(var employee in employeesResponse.Data)
                {
                    var employeePaychecks = (await GetEmployeePaychecks(employee.Id, year)).Data;
                    if (employeePaychecks != null)
                    paychecks.AddRange(employeePaychecks);
                }
            }
            else
            {
                paychecks = null;
            }

            var result = new ApiResponse<List<Paycheck>>()
            {
                Data = paychecks,
                Success = employeesResponse.Success,
                Error = employeesResponse.Error,
                Message = employeesResponse.Message
            };

            return await Task.FromResult(result);
        }
    }
}
