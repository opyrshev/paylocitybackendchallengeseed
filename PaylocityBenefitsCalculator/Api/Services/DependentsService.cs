﻿using Api.Dtos.Dependent;
using Api.Models;

namespace Api.Services
{
    public class DependentsService : IDependentsService
    {
        private static List<Dependent> dependents = new()
        {
            new()
            {
                Id = 1,
                FirstName = "Spouse",
                LastName = "Morant",
                Relationship = Relationship.Spouse,
                DateOfBirth = new DateTime(1998, 3, 3)
            },
            new()
            {
                Id = 2,
                FirstName = "Child1",
                LastName = "Morant",
                Relationship = Relationship.Child,
                DateOfBirth = new DateTime(2020, 6, 23)
            },
            new()
            {
                Id = 3,
                FirstName = "Child2",
                LastName = "Morant",
                Relationship = Relationship.Child,
                DateOfBirth = new DateTime(2021, 5, 18)
            },
            new()
            {
                Id = 4,
                FirstName = "DP",
                LastName = "Jordan",
                Relationship = Relationship.DomesticPartner,
                DateOfBirth = new DateTime(1974, 1, 2)
            }
        };

        public async Task<ApiResponse<GetDependentDto>> GetDependentResponse(int id)
        {
            var result = new ApiResponse<GetDependentDto>();
            var dependentDtos = dependents.Where(x => x.Id == id).Select(x => new GetDependentDto()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                DateOfBirth = x.DateOfBirth,
                Relationship = x.Relationship,
            }).FirstOrDefault();

            if (dependentDtos != null)
            {
                result.Data = dependentDtos;
                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.Error = "Not Found";
            }

            return await Task.FromResult(result);
        }

        public async Task<ApiResponse<List<GetDependentDto>>> GetDependentResponses()
        {
            var dependentDtos = dependents.Select(x => new GetDependentDto()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                DateOfBirth = x.DateOfBirth,
                Relationship = x.Relationship,
            }).ToList();

            var result = new ApiResponse<List<GetDependentDto>>()
            {
                Data = dependentDtos,
                Success = true
            };


            return await Task.FromResult(result);
        }
    }
}
