﻿using Api.Dtos.Employee;
using Api.Models;

namespace Api.Services
{
    public interface IEmployeesService
    {
        Task<ApiResponse<GetEmployeeDto>> GetEmployeeResponse(int id);

        Task<ApiResponse<List<GetEmployeeDto>>> GetEmployeeResponses();
    }
}
