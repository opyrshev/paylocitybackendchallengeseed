﻿using Api.Models;

namespace Api.Services
{
    public interface IPaycheckServise
    {
        Task<ApiResponse<List<Paycheck>>> GetEmployeePaychecks(int id, int year);

        Task<ApiResponse<List<Paycheck>>> GetEmployeesPaychecks(int year);
    }
}
