﻿namespace Api.Models
{
    public class Paycheck
    {
        public int Id { get; set; }

        public DateTime DateOfCalculation { get; set; }

        public int EmployeeId { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public decimal PaySum { get; set; }

        public decimal BaseCost { get; set; }

        public decimal DependentsCost { get; set; }

        public decimal ExceedingCost { get; set; }

        public decimal DependentAgeCost { get; set;}
    }
}
